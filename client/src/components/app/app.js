import Header       from '../header/header'
import { observer } from 'mobx-react-lite'
import Table        from '../table/table'

export default observer(() => {
  return (
    <div className='App'>
      <Header />
      <Table />
    </div>
  )
})

