import { makeAutoObservable } from 'mobx'

class Store {
  constructor () {
    makeAutoObservable(this, {}, { deep: true })
  }

  state = { data: [] }

  loadData = async () => {
    const response = await fetch('api/load')
    this.state.data = await response.json()
  }

  uploadData = async () => {
    const response = await fetch('api/upload')
    alert('Загрузка данных началась :)')
  }

  clear = () => this.state.data = []

}

export default new Store()