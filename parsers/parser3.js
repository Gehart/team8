module.exports =  function parse(data) {
    let products = [];
    const columnInfo = {
        name: 'A',
        count: 'B',
        price: 'C',
    }

    const sheetNames = data.SheetNames;

    for (sheet of sheetNames) {
        // в данном файле названия страниц являются названиями компании
        const brand = sheet;
        const workingSheet = data.Sheets[sheet];

        // const startRow = findFirstProduct(workingSheet, columnInfo);
        const endRow = findLastRowInSheet(workingSheet);

        let count = 0;
        while(count < endRow) {
            let obj = findNextProductAndModification(workingSheet, columnInfo, count);

            count = obj.count++;
            obj.products.forEach(el => el.brand = brand);
            products = products.concat(obj.products);
        }

        // break
    }
    return products;
}

function findNextProductAndModification(workingSheet, columnInfo, count) {
    // находим продукт и модификации (первая цена после count)
    // добавляем этот массив в products
    // 
    let products = [];
    let price;
    // находим продукт и его модификации
    price = getCellValue(workingSheet, columnInfo.price, count)
    while (!price || price === 'Цена') {
        count++;
        price = getCellValue(workingSheet, columnInfo.price, count)
    }
    const name = getCellValue(workingSheet, columnInfo.name, count - 1);

    while (price = getCellValue(workingSheet, columnInfo.price, count)) {
        let product = {}
        product.name = name + ' ' + getCellValue(workingSheet, columnInfo.name, count);
        product.count = Number(getCellValue(workingSheet, columnInfo.count, count));
        product.price = Number(price.split(/\s+/).join(''))

        products.push(product);
        count++;
    }

    return { products, count }
}

function findLastRowInSheet(workingSheet) {
    const rangeInSheet = workingSheet["!ref"];
    const lastRow = rangeInSheet
        .split(':')[1]   // последняя ячейка
        .split('')
        .filter(el => /[0-9]/.test(el))   // отделяем буквы (адрес колонки) от чисел
        .join('');
    
    return lastRow;
}

function getCellValue(workingSheet, columnAddress, rowAddress) {
    let cellValue = workingSheet[columnAddress + '' + rowAddress];

    // если в ячейке есть значение
    if (typeof cellValue != undefined && !!cellValue) {
        // возвращаем значение, избавляясь от лишних пробелов
        return (cellValue.v + '').trim().split(/\s+/).join(' ');
    }
    else {
        return null;
    }

}