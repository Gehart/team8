const { Router } = require('express')
const getData = require('../parser')
const fetch = require('node-fetch')
const Moysklad = require('moysklad')
const fs = require('fs')
const router = Router()
const moysklad = Moysklad({
  fetch, login: 'admin@nevainero', password: '688c6094d8'
})

router.get('/load', async (req, res) => {
  try {
    const data = getData()
    res.json(data)
  } catch (e) {
    console.log(e)
    res.status(500).json({ message: 'Ошибка сервера' })
  }
})

router.get('/upload', async (req, res) => {
  try {
    fs.readFile('./data/result.json', 'utf8', (err, data) => {
      if (err) throw err
      data = JSON.parse(data)
      let counter = 0
      const interval = setInterval(async () => {
        if (counter === data.length) clearInterval(interval)
        const result = await moysklad.POST('entity/product', data[counter++])
        console.log(result)
      }, 200)

    })
  } catch (e) {
    console.log(e)
    res.status(500).json({ message: 'Ошибка сервера' })
  }
})

module.exports = router